// -------------------------------------------------------------------------------------- config and strings
const config = {
  eventSourceURL: '/api/on-visitor-change',
  getFloorsURL: '/api/floors',
  reportTo: 'Admin Tower',
}

const strings = {
  maxCap: 'Max Capacity',
  currHeadcount: 'Current Headcount',
}

var data = {
  floors: [],
}

// -------------------------------------------------------------------------------------- app
const app = {
  start: async () => {
    await app.fetchAndSetData()
    app.serverSentEvent()
    app.refreshView()
  },
  fetchAndSetData: async () => {
    const request = await fetch(config.getFloorsURL)
    const rows = await request.json()
    data.floors = rows
  },
  serverSentEvent: () => {
    if (!window.EventSource) return alert("Your browser doesn't support SSE")

    const source = new EventSource(config.eventSourceURL)

    const onMessage = function (event) {
      const data = JSON.parse(event.data)
      if (data.eventType === 'updateFloor') {
        app.upsertFloor(data.floor)
        app.refreshView()
      }
    }

    const onOpen = function (event) {
      console.log('onOpen', +new Date())
    }

    const onError = function (e) {
      if (e.eventPhase == EventSource.CLOSED) {
        // source.close()
      }

      if (e.target.readyState == EventSource.CLOSED) {
        console.log('Disconnected', +new Date())
      } else if (e.target.readyState == EventSource.CONNECTING) {
        console.log('Disconnected', +new Date())
      }
    }

    source.addEventListener('message', onMessage, false)
    source.addEventListener('open', onOpen, false)
    source.addEventListener('error', onError, false)
  },
  upsertFloor: (newFloor) => {
    var index = 0
    for (let floor of data.floors) {
      console.log('floor.id', floor.id)
      console.log('newFloor', newFloor)
      if (floor.id == newFloor.id) {
        data.floors[index] = newFloor
        console.log('index', index)
        console.log('newFloor', newFloor)
      }
      index++
    }
  },
  refreshView: () => {
    const setDom = () => {
      function addCard(floor) {
        const html = `<div class="card">
        <p>${strings.maxCap}: ${floor.maxCapacity}</p>
        <p>${strings.currHeadcount}: ${floor.currentCapacity}</p>
      </div>`
        document.querySelector('#right-sidebar #cards').innerHTML =
          document.querySelector('#right-sidebar #cards').innerHTML + html
      }

      function addCell(progressPercent) {
        const reverseProgress = progressPercent > 100 ? 0 : Math.abs(progressPercent - 100)
        const html = `
        <div class="progress-bar-container">
          <div class="progress-bar-child progress"></div>
          <div class="progress-bar-child shrinker timelapse" style="width: ${reverseProgress.toString()}%"></div>
        </div>
      `

        document.querySelector('#left-sidebar').innerHTML =
          document.querySelector('#left-sidebar').innerHTML + html
      }

      // reset view
      document.querySelector('#left-sidebar').innerHTML = ''
      document.querySelector('#right-sidebar #cards').innerHTML = ''

      for (let floor of data.floors) {
        addCard(floor)
        addCell((floor.currentCapacity / floor.maxCapacity) * 100)
      }
    }

    const calculateDisplay = () => {
      const cards = document.querySelectorAll('.card')

      var boxSize, fontSize
      if (cards.length < 5) {
        boxSize = 40
      } else if (cards.length < 10) {
        boxSize = 20
      } else if (cards.length < 30) {
        boxSize = 15
      } else {
        boxSize = 10
      }

      if (cards.length < 5) {
        fontSize = '2vw'
      } else if (cards.length < 10) {
        fontSize = '1.8vw'
      } else if (cards.length < 25) {
        fontSize = '1.2vw'
      } else {
        fontSize = '0.8vw'
      }

      const $cards = document.querySelector('#content > #right-sidebar #cards')
      $cards.style['grid-template-columns'] = `repeat(auto-fill, minmax(${boxSize}vw, 1fr))`

      for (let i = 0; i < document.querySelectorAll('.card').length; i++) {
        document.querySelectorAll('.card')[i].style['font-size'] = fontSize
      }

      if (document.querySelectorAll('.progress-bar-container').length) {
        for (let progressContainer of document.querySelectorAll('.progress-bar-container')) {
          progressContainer.style.top = cards.length > 20 ? '1vh' : '3vh'
        }
      }
    }

    const showAlertIfBreach = () => {
      const hideAlertAfter = 5000

      const hideAlertDom = () => (document.querySelector('#alert').style.display = 'none')
      const showAlertDom = (maxCap = 0, floorName = 'Floor 1') => {
        document.querySelector('#alert-floor').innerHTML = floorName
        document.querySelector('#alert-max-cap').innerHTML = maxCap.toString()
        document.querySelector('#alert-show-to').innerHTML = config.reportTo
        document.querySelector('#alert').style.display = 'block'

        setTimeout(hideAlertDom, hideAlertAfter)
      }

      var isBreach = false
      for (let floor of data.floors) {
        // return early
        if (floor.currentCapacity > floor.maxCapacity) {
          showAlertDom(floor.maxCapacity, floor.name)
          return
        }
      }

      hideAlertDom()
    }

    setDom()
    calculateDisplay()
    showAlertIfBreach()
  },
  testView: () => {
    const generateFloor = () => {
      return {
        id: 10,
        name: 'Floor Random',
        currentCapacity: 5,
        maxCapacity: 50,
        createdAt: '2020-08-25T16:54:24.000Z',
        updatedAt: '2020-08-09T09:56:07.000Z',
      }
    }

    data.floors.push(generateFloor())
    app.refreshView()
  },
}

app.start()
