import * as React from 'react'
import { List, Datagrid, TextField, DateField, EmailField, Edit, Create, ReferenceField } from 'react-admin'

const ListComponent = (props) => (
  <List {...props} bulkActionButtons={false}>
    <Datagrid rowClick="">
      <TextField source="id" sortable={false} />
      <ReferenceField label="Employee" source="employeeId" reference="employees" sortable={false}>
        <TextField source="name" />
      </ReferenceField>
      <ReferenceField label="Floor" source="floorId" reference="floors" sortable={false}>
        <TextField source="name" />
      </ReferenceField>
      <TextField source="inOrOut" sortable={false} />
      <DateField source="createdAt" showTime={true} sortable={false} />
    </Datagrid>
  </List>
)

export default ListComponent
