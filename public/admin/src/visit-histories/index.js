import BookIcon from '@material-ui/icons/History'
import List from './List'

export default {
  list: List,
  icon: BookIcon,
}
