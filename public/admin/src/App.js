import * as React from 'react'
import { Admin, Resource, ListGuesser, ShowGuesser, fetchUtils } from 'react-admin'
import jsonServerProvider from 'ra-data-json-server'
import authProvider from './authProvider'
import config from './config'
import admins from './admins/index'
import employees from './employees/index'
import floors from './floors/index'
import visitHistories from './visit-histories/index'

const httpClient = (url, options = {}) => {
  options.user = {
    authenticated: true,
    token: localStorage.getItem('token'),
  }
  return fetchUtils.fetchJson(url, options)
}

const dataProvider = jsonServerProvider(config.host, httpClient)
const App = () => (
  <Admin dataProvider={dataProvider} authProvider={authProvider}>
    <Resource name="admins" {...admins} />
    <Resource name="employees" {...employees} />
    <Resource name="floors" {...floors} />
    <Resource name="visit-histories" {...visitHistories} />
  </Admin>
)

export default App
