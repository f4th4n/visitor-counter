import * as React from 'react'
import { List, Datagrid, TextField, DateField, EmailField, Edit, Create } from 'react-admin'

const ListComponent = (props) => (
  <List {...props} bulkActionButtons={false}>
    <Datagrid rowClick="edit">
      <TextField source="id" sortable={false} />
      <TextField source="name" sortable={false} />
      <TextField source="currentCapacity" sortable={false} />
      <DateField source="maxCapacity" sortable={false} />
      <DateField source="createdAt" sortable={false} />
    </Datagrid>
  </List>
)

export default ListComponent
