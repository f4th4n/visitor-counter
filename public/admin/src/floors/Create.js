import * as React from 'react'
import {
  List,
  Datagrid,
  TextField,
  DateField,
  EmailField,
  Edit,
  Create,
  SimpleForm,
  TextInput,
  PasswordInput,
} from 'react-admin'

const AdminEdit = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput disabled source="id" />
      <TextInput source="name" />
      <TextInput source="currentCapacity" />
      <TextInput source="maxCapacity" />
    </SimpleForm>
  </Create>
)

export default AdminEdit
