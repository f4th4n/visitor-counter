import * as React from 'react'
import {
  List,
  Datagrid,
  TextField,
  DateField,
  EmailField,
  Edit,
  Create,
  SimpleForm,
  TextInput,
  PasswordInput,
} from 'react-admin'

const AdminEdit = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput disabled source="id" />
      <TextInput source="username" />
      <PasswordInput source="password" />
    </SimpleForm>
  </Create>
)

export default AdminEdit
