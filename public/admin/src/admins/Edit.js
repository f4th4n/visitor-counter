import * as React from 'react'
import {
  List,
  Datagrid,
  TextField,
  DateField,
  EmailField,
  Edit,
  Create,
  SimpleForm,
  TextInput,
  PasswordInput,
} from 'react-admin'

const AdminEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput disabled source="id" />
      <TextInput source="username" />
      <PasswordInput source="password" />
    </SimpleForm>
  </Edit>
)

export default AdminEdit
