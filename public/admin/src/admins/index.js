import BookIcon from '@material-ui/icons/VerifiedUser'
import Create from './Create'
import Edit from './Edit'
import List from './List'

export default {
  list: List,
  create: Create,
  edit: Edit,
  icon: BookIcon,
}
