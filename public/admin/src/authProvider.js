import config from './config'

export default {
  // called when the user attempts to log in
  login: async ({ username, password }) => {
    var formBody = []
    formBody.push('username=' + encodeURIComponent(username))
    formBody.push('password=' + encodeURIComponent(password))
    formBody = formBody.join('&')

    try {
      const res = await fetch(config.host + '/login', {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' },
        method: 'POST',
        mode: 'cors',
        body: formBody,
      })
      const resBody = await res.json()
      localStorage.setItem('token', resBody.accessToken)
      Promise.resolve()
    } catch (e) {
      Promise.reject()
    }
  },
  // called when the user clicks on the logout button
  logout: () => {
    localStorage.removeItem('token')
    return Promise.resolve()
  },
  // called when the API returns an error
  checkError: ({ status }) => {
    if (status === 401 || status === 403) {
      localStorage.removeItem('token')
      return Promise.reject()
    }
    return Promise.resolve()
  },
  // called when the user navigates to a new location, to check for authentication
  checkAuth: () => {
    return localStorage.getItem('token') ? Promise.resolve() : Promise.reject()
  },
  // called when the user navigates to a new location, to check for permissions / roles
  getPermissions: () => Promise.resolve(),
}
