import * as React from 'react'
import { List, Datagrid, TextField, DateField, EmailField, Edit, Create } from 'react-admin'

const AdminList = (props) => (
  <List {...props} bulkActionButtons={false}>
    <Datagrid rowClick="edit">
      <TextField source="id" sortable={false} />
      <TextField source="employeeId" sortable={false} />
      <TextField source="name" sortable={false} />
      <DateField source="createdAt" sortable={false} />
    </Datagrid>
  </List>
)

export default AdminList
