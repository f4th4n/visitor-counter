# Visitor Counter

## Installation

1. copy .env.example to .env
2. modify .env
3. npm install

## Start Server

Development mode

```
npm run dev
```

Production mode

```
npm run prod
```

### API Documentation

open http://localhost:8800/api-docs

### Display Visitor Counter Page

open http://localhost:8800/display-visitor-counter

### Admin Page

Go to folder public/admin

1. Install dependency

```
$ npm install
```

2. modify public/admin/src/config.json

3. build

```
$ npm run build
```

4. serve the html at public/admin/build

## Reset Visitor Counter

Put this command in cron

```
$ npm run reset-counter
```

## Seed data (for development)

```
$ npm run seed
```

# Auth

default
username: admin1
password: 9dfdo2k3o4vncdsk
