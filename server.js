const dotenv = require('dotenv')
const https = require('https')
const fs = require('fs')
const express = require('express')
const cors = require('cors')

dotenv.config()
const { assertDbConnection } = require('./app/models')
const app = express()

const bodyParser = require('body-parser')
const authMiddleware = require('./app/middleware/auth')

const moment = require('moment-timezone')
moment().tz('Asia/Jakarta').format()

app.use(cors())

app.use(express.static('public'))

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

// define a simple route
app.get('/', (req, res) => {
  res.json({ message: 'Welcome to APIs.' })
})

app.use(authMiddleware)
require('./app/swagger.js')(app)
require('./app/routes/client.routes.js')(app)
require('./app/routes/admin.routes.js')(app)
//require('./app/routes/auth.routes.js')(app)

port = process.env.LISTEN_PORT || 3000

const init = async () => {
  await assertDbConnection()
  app.listen(port, () => console.log(`Server is listening on port ${port}`))
}

init()
