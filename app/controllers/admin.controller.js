const jwt = require('jsonwebtoken')
const { models } = require('../models').sequelize
const config = require('../../config')
const moment = require('moment')
const helper = require('../helper')

// make a data safer with data transfer object
const dto = (admin) => {
  delete admin.password
  return admin
}

// -------------------------------------------------------------------------------------- module expose

exports.login = async (req, res) => {
  const { username, password } = req.body
  const rowClient = await models.admin.findOne({ where: { username, password } })
  if (!rowClient) return res.status(401).send({ code: 401, message: 'Wrong authentication' })

  const jwtData = { id: rowClient.id }
  const token = jwt.sign(jwtData, process.env.JWT_SECRET || 'secretsecret')

  const expiredAt = moment().add(config.tokenExpiredInMinutes, 'minutes')
  const rowToken = await models.token.create({ accessToken: token, expiredAt })

  res.json(rowToken)
}

exports.create = async (req, res) => {
  const { username, password } = req.body
  try {
    const admin = await models.admin.create({ username, password })
    res.json(admin)
  } catch (e) {
    res.status(422).json({ code: 422, message: e.message })
  }
}

exports.udpate = async (req, res) => {
  const { username, password } = req.body
  try {
    const admin = await models.admin.findByPk(req.params.id)
    if (username) admin.username = username
    if (password) admin.password = password

    await admin.save()

    res.json(admin)
  } catch (e) {
    res.status(422).json({ code: 422, message: e.message })
  }
}

exports.get = async (req, res) => {
  const admin = await models.admin.findByPk(req.params.id)
  if (!admin) return res.status(404).send({ code: 404, message: 'Not found' })

  return res.json(dto(admin.toJSON()))
}

exports.find = async (req, res) => {
  const admins = await models.admin.findAll(helper.pagination(req))
  res.header('Access-Control-Expose-Headers', 'X-Total-Count')
  res.header('X-Total-Count', await models.admin.count())
  res.json(admins.map((admin) => dto(admin.toJSON())))
}

exports.delete = async (req, res) => {
  const admin = await models.admin.findByPk(req.params.id)
  if (!admin) res.status(404).send({ code: 404, message: 'Not found' })

  await admin.destroy()
  res.json({ status: 'success' })
}
