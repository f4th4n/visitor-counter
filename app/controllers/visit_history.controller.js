const { models } = require('../models').sequelize
const helper = require('../helper')

const dto = (visitHistory) => {
  return visitHistory
}

exports.find = async (req, res) => {
  const visitHistories = await models.visitHistory.findAll(helper.pagination(req))
  res.header('Access-Control-Expose-Headers', 'X-Total-Count')
  res.header('X-Total-Count', await models.visitHistory.count())
  res.json(visitHistories.map((visitHistory) => dto(visitHistory.toJSON())))
}
