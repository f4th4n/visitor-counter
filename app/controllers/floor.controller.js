const { models } = require('../models').sequelize
const event = require('../event')
const helper = require('../helper')

const dto = (floor) => {
  return floor
}

const getTemperature = (temperature) => {
  const isFloat = (val) => +val === val && (!isFinite(val) || !!(val % 1))
  var isTemperatureFloat = isFloat(parseFloat(temperature))
  return isTemperatureFloat ? parseFloat(temperature) : parseInt(temperature)
}

const validateIncrementDecrement = (req) => {
  if (!req.body.floorId) return { code: 400, message: 'floorId is missing' }
  else if (!req.body.temperature) return { code: 400, message: 'temperature is missing' }
  else if (!getTemperature(req.body.temperature)) return { code: 400, message: 'temperature bad format' }
}

const createHistory = async (req, floor, inOrOut) => {
  const history = models.visitHistory.build({
    employeeId: req.body.employeeId || null,
    floorId: floor.id,
    temperature: getTemperature(req.body.temperature),
    inOrOut,
  })
  await history.save()
}

// -------------------------------------------------------------------------------------- module expose

exports.incrementCapacity = async (req, res) => {
  const validation = validateIncrementDecrement(req)
  if (validation) return res.status(validation.code).send(validation)

  const floor = await models.floor.findByPk(req.body.floorId)
  if (!floor) return res.status(404).send({ code: 404, message: 'Not found' })

  createHistory(req, floor, 'in')
  if (!req.body.employeeId) return res.status(200).send({ code: 200, message: 'Non employee, skip' })

  const employee = await models.employee.findByPk(req.body.employeeId)
  if (!employee) return res.status(404).send({ code: 404, message: 'Not Found' })

  floor.currentCapacity++
  floor.save()

  res.json(floor)
  event.emit('updateFloor', floor.toJSON())
}

exports.decrementCapacity = async (req, res) => {
  const validation = validateIncrementDecrement(req)
  if (validation) return res.status(validation.code).send(validation)

  const floor = await models.floor.findByPk(req.body.floorId)
  if (!floor) return res.status(404).send({ code: 404, message: 'Not found' })

  createHistory(req, floor, 'out')
  if (!req.body.employeeId) return res.status(200).send({ code: 200, message: 'Non employee, skip' })

  const employee = await models.employee.findByPk(req.body.employeeId)
  if (!employee) return res.status(404).send({ code: 404, message: 'Employee Not found' })

  floor.currentCapacity--
  if (floor.currentCapacity < 0) floor.currentCapacity = 0
  floor.save()

  res.json(floor)
  event.emit('updateFloor', floor.toJSON())
}

exports.getData = async (req, res) => {
  const floors = await models.floor.findAll()
  return res.json(floors)
}

exports.create = async (req, res) => {
  const { currentCapacity, maxCapacity, name } = req.body
  try {
    const floor = await models.floor.create({ currentCapacity, maxCapacity, name })
    res.json(floor)
  } catch (e) {
    res.status(422).json({ code: 422, message: e.message })
  }
}

exports.udpate = async (req, res) => {
  const { currentCapacity, maxCapacity, name } = req.body
  try {
    const floor = await models.floor.findByPk(req.params.id)
    if (currentCapacity) floor.currentCapacity = currentCapacity
    if (maxCapacity) floor.maxCapacity = maxCapacity
    if (name) floor.name = name

    await floor.save()
    event.emit('updateFloor', floor.toJSON())

    res.json(floor)
  } catch (e) {
    res.status(422).json({ code: 422, message: e.message })
  }
}

exports.get = async (req, res) => {
  const floor = await models.floor.findByPk(req.params.id)
  if (!floor) return res.status(404).send({ code: 404, message: 'Not found' })

  return res.json(dto(floor.toJSON()))
}

exports.find = async (req, res) => {
  const floors = await models.floor.findAll(helper.pagination(req))
  res.header('Access-Control-Expose-Headers', 'X-Total-Count')
  res.header('X-Total-Count', await models.floor.count())
  res.json(floors.map((floor) => dto(floor.toJSON())))
}

exports.delete = async (req, res) => {
  const floor = await models.floor.findByPk(req.params.id)
  if (!floor) res.status(404).send({ code: 404, message: 'Not found' })

  await floor.destroy()
  res.json({ status: 'success' })
}
