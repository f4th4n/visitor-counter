const jwt = require('jsonwebtoken')
const { models } = require('../models').sequelize
const config = require('../../config')
const moment = require('moment')
const helper = require('../helper')

const dto = (employee) => {
  return employee
}

exports.create = async (req, res) => {
  const { employeeId, name } = req.body
  try {
    const employee = await models.employee.create({ employeeId, name })
    res.json(employee)
  } catch (e) {
    res.status(422).json({ code: 422, message: e.message })
  }
}

exports.udpate = async (req, res) => {
  const { employeeId, name } = req.body
  try {
    const employee = await models.employee.findByPk(req.params.id)
    if (employeeId) employee.employeeId = employeeId
    if (name) employee.name = name

    await employee.save()

    res.json(employee)
  } catch (e) {
    res.status(422).json({ code: 422, message: e.message })
  }
}

exports.get = async (req, res) => {
  const employee = await models.employee.findByPk(req.params.id)
  if (!employee) return res.status(404).send({ code: 404, message: 'Not found' })

  return res.json(dto(employee.toJSON()))
}

exports.find = async (req, res) => {
  const employees = await models.employee.findAll(helper.pagination(req))
  res.header('Access-Control-Expose-Headers', 'X-Total-Count')
  res.header('X-Total-Count', await models.employee.count())
  res.json(employees.map((employee) => dto(employee.toJSON())))
}

exports.delete = async (req, res) => {
  const employee = await models.employee.findByPk(req.params.id)
  if (!employee) res.status(404).send({ code: 404, message: 'Not found' })

  await employee.destroy()
  res.json({ status: 'success' })
}
