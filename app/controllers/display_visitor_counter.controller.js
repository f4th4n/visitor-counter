const { models } = require('../models').sequelize
const event = require('../event')

function writeResponse(res, floors) {
  event.on('updateFloor', async function (floor) {
    const data = { floor, eventType: 'updateFloor' }
    res.write('data: ' + JSON.stringify(data) + '\n\n')
  })
}

exports.onVisitorChange = async (req, res) => {
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    Connection: 'keep-alive',
  })
  writeResponse(res)
}
