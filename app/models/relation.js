function applyRelation(sequelize) {
  const { visitHistory, employee, floor } = sequelize.models

  employee.hasMany(visitHistory)
  visitHistory.belongsTo(employee)
  visitHistory.belongsTo(floor)
}

module.exports = applyRelation
