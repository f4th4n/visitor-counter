const Sequelize = require('sequelize')
const applyRelation = require('./relation')

const options = {
  host: process.env.SQL_SERVER,
  port: process.env.SQL_PORT,
  dialect: 'mysql',
  logging: false,
}

const sequelize = new Sequelize(
  process.env.SQL_DATABASE,
  process.env.SQL_USER,
  process.env.SQL_PASSWORD,
  options
)

const models = [
  require('./employee.model'),
  require('./admin.model'),
  require('./visit_history.model'),
  require('./floor.model'),
  require('./config.model'),
  require('./token.model'),
]

for (let model of models) {
  model(sequelize)
}

// create foreign-key
applyRelation(sequelize)

// connect to database, fail early on error
async function assertDbConnection() {
  try {
    await sequelize.authenticate()
    await sequelize.sync()
  } catch (error) {
    console.log('Unable to connect to the database:')
    console.log(error.message)
    process.exit(1)
  }
}

module.exports = { sequelize, assertDbConnection }
