const { DataTypes } = require('sequelize')

module.exports = (sequelize) => {
  sequelize.define(
    'floor',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      currentCapacity: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      maxCapacity: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      underscored: true,
    }
  )
}
