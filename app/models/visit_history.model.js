const { DataTypes } = require('sequelize')

module.exports = (sequelize) => {
  sequelize.define(
    'visitHistory',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      employeeId: {
        type: DataTypes.INTEGER,
      },
      floorId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      temperature: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      inOrOut: {
        type: DataTypes.ENUM('in', 'out'),
        allowNull: false,
      },
    },
    {
      underscored: true,
    }
  )
}
