const { DataTypes } = require('sequelize')

module.exports = (sequelize) => {
  sequelize.define(
    'admin',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: {
            args: true,
            msg: 'username required',
          },
          is: {
            args: ['^[a-z1-9]+$', 'i'],
            msg: 'only alpha numeric allowed in username',
          },
          len: {
            args: [3, 64],
            msg: 'username min 3 max 64',
          },
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: {
            args: true,
            msg: 'password required',
          },
          len: {
            args: [6, 64],
            msg: 'password min 6 max 64',
          },
        },
      },
    },
    {
      underscored: true,
    }
  )
}
