const helper = {
  pagination: (req) => {
    return {
      limit: 10,
      offset: parseInt(req.query._start) || 0,
    }
  },
}

module.exports = helper
