const jwt = require('jsonwebtoken')
const moment = require('moment')
const { models } = require('../models').sequelize
const config = require('../../config')

const getRowToken = async (req) => {
  if (!req.headers.authorization) return null
  const bearer = req.headers.authorization.replace('Bearer ', '')
  if (!bearer) return

  return await models.token.findOne({ where: { accessToken: bearer } })
}

const getTokenPayload = (rowToken) => {
  const decoded = jwt.decode(rowToken.accessToken)
  if (!decoded) return null
  return decoded
}

const isExpired = async (token) => {
  const now = moment()
  return moment(token.expiredAt).isSameOrBefore(now)
}

const extendExpiration = async (rowToken) => {
  const expiredAt = moment().add(config.tokenExpiredInMinutes, 'minutes')
  rowToken.expiredAt = expiredAt
  await rowToken.save()
}

// -------------------------------------------------------------------------------------- module expose

module.exports = async (req, res, next) => {
  const rowToken = await getRowToken(req)
  if (!rowToken) return next()

  const expired = await isExpired(rowToken)
  if (expired) {
    return res.status(498).send({
      code: 498,
      message: 'Token expired',
    })
  }

  const payload = getTokenPayload(rowToken)
  const admin = await models.admin.findByPk(payload.id)
  req.admin = admin.toJSON()
  extendExpiration(rowToken)
  next()
}
