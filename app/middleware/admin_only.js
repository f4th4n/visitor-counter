module.exports = (req, res, next) => {
  if (!req.admin) {
    return res.status(403).send({
      code: 403,
      message: 'Forbidden',
    })
  }

  return next()
}
