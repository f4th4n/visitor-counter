const admin = require('../controllers/admin.controller')
const employee = require('../controllers/employee.controller')
const floor = require('../controllers/floor.controller')
const visitHistory = require('../controllers/visit_history.controller')
const adminOnly = require('../middleware/admin_only')
const { models } = require('../models').sequelize
const { crud } = require('express-sequelize-crud')

module.exports = (app) => {
  /**
   * @swagger
   *
   * /login:
   *   post:
   *     tags:
   *      - Admin
   *     description: Login to the application
   *     produces:
   *       - application/json
   *     requestBody:
   *       content:
   *         application/x-www-form-urlencoded:
   *           schema:
   *             type: object
   *             properties:
   *               username:
   *                 type: string
   *               password:
   *                 type: string
   *     responses:
   *       200:
   *         description: login
   */
  app.post('/api/admin/login', admin.login)

  app.post('/api/admin/admins', adminOnly, admin.create)
  app.put('/api/admin/admins/:id', adminOnly, admin.udpate)
  app.get('/api/admin/admins/:id', adminOnly, admin.get)
  app.get('/api/admin/admins', adminOnly, admin.find)
  app.delete('/api/admin/admins/:id', adminOnly, admin.delete)

  app.post('/api/admin/employees', adminOnly, employee.create)
  app.put('/api/admin/employees/:id', adminOnly, employee.udpate)
  app.get('/api/admin/employees/:id', adminOnly, employee.get)
  app.get('/api/admin/employees', adminOnly, employee.find)
  app.delete('/api/admin/employees/:id', adminOnly, employee.delete)

  app.post('/api/admin/floors', adminOnly, floor.create)
  app.put('/api/admin/floors/:id', adminOnly, floor.udpate)
  app.get('/api/admin/floors/:id', adminOnly, floor.get)
  app.get('/api/admin/floors', adminOnly, floor.find)
  app.delete('/api/admin/floors/:id', adminOnly, floor.delete)

  app.get('/api/admin/visit-histories', adminOnly, visitHistory.find)
}
