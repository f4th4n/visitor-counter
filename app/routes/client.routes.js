const floor = require('../controllers/floor.controller.js')
const displayVisitorCounter = require('../controllers/display_visitor_counter.controller.js')

/*
	note:
		unauthenticated request goes here
*/

module.exports = (app) => {
	// ------------------------------------------------------------------------------------ rest API

	/**
	 * @swagger
	 * /increment-capacity:
	 *   post:
	 *     summary: Increase current capacity
	 *     tags:
	 *      - Guest
	 *     security:
	 *      - bearerAuth: []
	 *     produces:
	 *      - application/json
	 *     requestBody:
	 *       content:
	 *         application/x-www-form-urlencoded:
	 *           schema:
	 *             type: object
	 *             properties:
	 *               employeeId:
	 *                 type: number
	 *                 example: 1
	 *               floorId:
	 *                 type: number
	 *                 example: 1
	 *               temperature:
	 *                 type: float
	 *                 example: 35.25
	 *             required:
	 *               - floorId
	 *               - temperature
	 *     responses:
	 *       200:
	 *         description: A successful response
	 *       400:
	 *         description: Required body is missing
	 *       404:
	 *         description: Data is missing
	 */
	app.post('/api/increment-capacity', floor.incrementCapacity)

	/**
	 * @swagger
	 * /decrement-capacity:
	 *   post:
	 *     summary: Reduce current capacity
	 *     tags:
	 *      - Guest
	 *     security:
	 *      - bearerAuth: []
	 *     produces:
	 *      - application/json
	 *     requestBody:
	 *       content:
	 *         application/x-www-form-urlencoded:
	 *           schema:
	 *             type: object
	 *             properties:
	 *               employeeId:
	 *                 type: number
	 *                 example: 1
	 *               floorId:
	 *                 type: number
	 *                 example: 1
	 *               temperature:
	 *                 type: float
	 *                 example: 35.25
	 *             required:
	 *               - floorId
	 *               - temperature
	 *     responses:
	 *       200:
	 *         description: A successful response
	 *       400:
	 *         description: Required body is missing
	 *       404:
	 *         description: Data is missing
	 */
	app.post('/api/decrement-capacity', floor.decrementCapacity)

	/**
	 * @swagger
	 * /floors:
	 *   get:
	 *     summary: Get all floors data
	 *     tags:
	 *      - Guest
	 *     security:
	 *      - bearerAuth: []
	 *     produces:
	 *      - application/json
	 *     responses:
	 *       200:
	 *         description: A successful response
	 *       400:
	 *         description: Required body is missing
	 *       404:
	 *         description: Data is missing
	 */
	app.get('/api/floors', floor.getData)

	// ------------------------------------------------------------------------------------ server sent event
	app.get('/api/on-visitor-change', displayVisitorCounter.onVisitorChange)
}
