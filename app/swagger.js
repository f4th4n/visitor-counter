const config = require('../config')
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')

module.exports = (app) => {
  // Extended: https://swagger.io/specification/#infoObject
  const swaggerOptions = {
    swaggerDefinition: {
      openapi: '3.0.0',
      info: {
        title: config.appName,
        version: '3.0.0',
        description: config.appName,
        contact: {
          name: 'Developer',
          email: config.developerEmail,
        },
      },
      servers: [
        {
          url: `http://localhost:${process.env.LISTEN_PORT}/api`,
          description: 'Local server',
        },
        {
          url: `${process.env.PROD_URL}/api`,
          description: 'Production server',
        },
      ],
      components: {
        securitySchemes: {
          bearerAuth: {
            type: 'http',
            scheme: 'bearer',
            bearerFormat: 'JWT',
          },
        },
        responses: {
          UnauthorizedError: {
            description: 'Access token is missing or invalid',
          },
        },
      },
      // security: {
      //   bearerAuth: []
      // },
    },
    apis: ['app/routes/*.js'],
    //apis: ["server.js"]
  }

  const swaggerDocument = swaggerJsDoc(swaggerOptions)

  var options = {
    customCss: '.swagger-ui .topbar { display: none }',
  }
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options))
}
