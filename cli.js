const command = process.argv[2]
const dotenv = require('dotenv')

dotenv.config()

const resetCounter = async () => {
  const { assertDbConnection } = require('./app/models')
  const { models } = require('./app/models').sequelize
  await assertDbConnection()

  await models.floor.update({ currentCapacity: 0 }, { where: {} })
  process.exit()
}

const seed = async () => {
  const floors = [
    { name: 'Floor Satu', currentCapacity: 7, maxCapacity: 30 },
    { name: 'Floor Dua', currentCapacity: 23, maxCapacity: 30 },
    { name: 'Floor Tiga', currentCapacity: 3, maxCapacity: 30 },
    { name: 'Floor Empat', currentCapacity: 19, maxCapacity: 30 },
    { name: 'Floor Lima', currentCapacity: 40, maxCapacity: 50 },
    { name: 'Floor Enam', currentCapacity: 28, maxCapacity: 50 },
    { name: 'Floor Tujuh', currentCapacity: 50, maxCapacity: 50 },
    { name: 'Floor Delapan', currentCapacity: 20, maxCapacity: 50 },
    { name: 'Floor Sembilan', currentCapacity: 42, maxCapacity: 50 },
    { name: 'Floor Sepuluh', currentCapacity: 5, maxCapacity: 50 },
    { name: 'Floor Sebelas', currentCapacity: 1, maxCapacity: 50 },
    { name: 'Floor Duabelas', currentCapacity: 22, maxCapacity: 50 },
    { name: 'Floor Tigabelas', currentCapacity: 42, maxCapacity: 50 },
    { name: 'Floor Empatbelas', currentCapacity: 44, maxCapacity: 50 },
  ]

  const employees = [
    { employeeId: 'ABC12345', name: 'Budi' },
    { employeeId: 'DEF45678', name: 'Ani' },
    { employeeId: 'GHSI234219', name: 'Cholil' },
  ]

  const admins = [{ username: 'admin1', password: '9dfdo2k3o4vncdsk' }]

  const { assertDbConnection } = require('./app/models')
  const { models } = require('./app/models').sequelize
  await assertDbConnection()

  for (let floor of floors) {
    const rowFloor = await models.floor.findOne({ where: { name: floor.name } })
    if (rowFloor) continue

    await models.floor.create(floor).catch((e) => {})
  }

  for (let employee of employees) {
    const rowEmployee = await models.employee.findOne({ where: { employeeId: employee.employeeId } })
    if (rowEmployee) continue

    await models.employee.create(employee).catch((e) => {})
  }

  for (let admin of admins) {
    const rowAdmin = await models.admin.findOne({ where: { username: admin.username } })
    if (rowAdmin) continue

    await models.admin.create(admin).catch((e) => {})
  }

  process.exit()
}

if (command === 'reset-counter') resetCounter()
else if (command === 'seed') seed()
else console.log('Unknown command')
